<?php

namespace Drupal\ad_content\Plugin\Field\FieldFormatter;

use Drupal\ad_content\Field\AdContentImageFormatterBase;

/**
 * Plugin implementation of the AD Image formatter.
 *
 * @FieldFormatter(
 *   id = "ad_content_image",
 *   label = @Translation("AD Image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 *
 * @internal
 */
class AdContentImageFormatter extends AdContentImageFormatterBase {
}
